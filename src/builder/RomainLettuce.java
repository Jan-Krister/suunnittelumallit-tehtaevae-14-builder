package builder;

public class RomainLettuce extends Burger {
	@Override
	public String getIngredient() {
		return "Romaine-lettuce";
	}
}
