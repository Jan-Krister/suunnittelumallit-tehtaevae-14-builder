package builder;

import java.util.ArrayList;
import java.util.Scanner;

public class McBurgerBuilder implements BuilderInterface {
	private ArrayList<Burger> mcBurgerArray = new ArrayList<>();
	private Scanner sc = new Scanner(System.in);
	private boolean running = true;

	@Override
	public void ShowItems() {
		System.out.println("Your burgers ingredients:");
		for (int i = 0; i < mcBurgerArray.size(); i++) {
			String tempI = mcBurgerArray.get(i).getIngredient();
			System.out.println(tempI);
		}
	}

	@Override
	public void makeBurger() {
		System.out.println("Choose your ingredients:");
		System.out.println("1 for Beef-patty,\n2 for Vegetarian-patty,");
		System.out.println("3 for Romaine-lettuce,\n4 for iceberg-lettuce.");
		System.out.println("5 for cheddar,\n6 for gouda.");
		System.out.println("Select 7 when done.");

		while (running) {

			int temp = Integer.parseInt(sc.nextLine());

			switch (temp) {
			case 1: {
				mcBurgerArray.add(new BeefPatty());
				System.out.println("Beef-patty added");
				break;
			}
			case 2: {
				mcBurgerArray.add(new VegetarianPatty());
				System.out.println("Vege-patty added");
				break;
			}
			case 3: {
				mcBurgerArray.add(new RomainLettuce());
				System.out.println("Romaine-lettuce added");
				break;
			}
			case 4: {
				mcBurgerArray.add(new IceBergLettuce());
				System.out.println("Iceberg-lettuce added");
				break;
			}
			case 5: {
				mcBurgerArray.add(new Cheddar());
				System.out.println("Cheddar added");
				break;
			}
			case 6: {
				mcBurgerArray.add(new Gouda());
				System.out.println("Gouda added");
				break;
			}
			case 7: {
				this.ShowItems();
				sc.close();
				running = false;
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + temp);
			}
		}

	}

	public ArrayList<Burger> getBurger() {
		return mcBurgerArray;
	}
}
