package builder;

import java.util.Scanner;

public class HeseBurgerBuilder implements BuilderInterface {
	private StringBuilder strb = new StringBuilder(100);
	private String ingredients;

	private Scanner sc = new Scanner(System.in);
	private boolean running = true;

	public String getIngredients() {
		return ingredients;
	}

	@Override
	public void ShowItems() {
		System.out.println(strb);
	}

	@Override
	public void makeBurger() {
		System.out.println("Choose your ingredients:");
		System.out.println("1 for Beef-patty,\n2 for Vegetarian-patty,");
		System.out.println("3 for Romaine-lettuce,\n4 for iceberg-lettuce.");
		System.out.println("5 for cheddar,\n6 for gouda.");
		System.out.println("Select 7 when done.");
		strb.append("Your burger ingredients are: ");
		while (running) {
			int temp = sc.nextInt();

			switch (temp) {
			case 1: {
				strb.append(new BeefPatty().getIngredient());
				strb.append(" ");
				System.out.println("Beef-patty added");
				break;
			}
			case 2: {
				strb.append(new VegetarianPatty().getIngredient());
				System.out.println("Vege-patty added");
				strb.append(" ");
				break;
			}
			case 3: {
				strb.append(new RomainLettuce().getIngredient());
				System.out.println("Romaine-lettuce added");
				strb.append(" ");
				break;
			}
			case 4: {
				strb.append(new IceBergLettuce().getIngredient());
				System.out.println("Iceberg-lettuce added");
				strb.append(" ");
				break;
			}
			case 5: {
				strb.append(new Cheddar().getIngredient());
				System.out.println("Cheddar added");
				strb.append(" ");
				break;
			}
			case 6: {
				strb.append(new Gouda().getIngredient());
				System.out.println("Gouda added");
				strb.append(" ");
				break;
			}
			case 7: {
				this.ShowItems();
				sc.close();
				running = false;
				break;
			}
			default:
				throw new IllegalArgumentException("Unexpected value: " + temp);
			}
		}

	}

	public StringBuilder getBurger() {
		return strb;
	}
}
