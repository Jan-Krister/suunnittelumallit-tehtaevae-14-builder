package builder;

public class Gouda extends Burger {
	@Override
	public String getIngredient() {
		return "Gouda";
	}
	@Override
	public String toString() {
		return "gouda";
	}
}
