package builder;

public abstract class Burger {

	public abstract String getIngredient();

}
