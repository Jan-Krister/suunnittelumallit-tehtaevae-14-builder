package builder;

public class IceBergLettuce extends Burger{
	@Override
	public String getIngredient() {
		return "Iceberg-Lettuce";
	}
}
