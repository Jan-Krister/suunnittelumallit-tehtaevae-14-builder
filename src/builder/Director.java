package builder;

public class Director {
	
	private BuilderInterface builder;

	public Director(BuilderInterface builder) {
		this.builder = builder;
	}

	public void changeBuilder(BuilderInterface builder) {
		this.builder = builder;
	}

	public void makeBurger(BuilderInterface builder) {
		builder.makeBurger();
	}

}
