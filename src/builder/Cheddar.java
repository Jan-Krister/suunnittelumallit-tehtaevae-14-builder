package builder;

public class Cheddar extends Burger {

	@Override
	public String getIngredient() {
		return "cheddar";
	}
}
