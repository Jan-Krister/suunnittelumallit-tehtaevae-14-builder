package builder;

public class BeefPatty extends Burger {

	@Override
	public String getIngredient() {
		return "Beef-patty";
	}
}
