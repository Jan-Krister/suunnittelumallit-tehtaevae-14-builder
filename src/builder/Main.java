package builder;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		BuilderInterface builder;
		Director director;
		Scanner sc = new Scanner(System.in);

		System.out.println("Would you like to have McDonalds or Hesburger?\n Choose 1 or 2 respectively");

		int option = sc.nextInt();

		if (option == 1) {
			builder = new McBurgerBuilder();
			director = new Director(builder);
			director.makeBurger(builder);
		} else {
			builder = new HeseBurgerBuilder();
			director = new Director(builder);
			director.makeBurger(builder);
		}
		sc.close();
	}
}
